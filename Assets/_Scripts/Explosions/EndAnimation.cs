﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Just handles end explosion behavior
public class EndAnimation : StateMachineBehaviour {

    //Destroy the explosion when done
	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Destroy(animator.gameObject);
	}

}
