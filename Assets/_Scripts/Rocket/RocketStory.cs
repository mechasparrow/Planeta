﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//rocket behavior for the story
public class RocketStory : MonoBehaviour {

    //grabs the animation controller for changing the current animation of the rocket
    public Animator anim;

    //boolean values for modifying the rockets current state
    public bool flying;
    private bool inspace;

    //Parent Rocket Man component for actual physical control of rocket
    private RocketMan rm;

    //field tracker to track gravitational fields that the rocket is effected by 
    private GravityFieldTracer gft;

    //checks if we collided with something
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if we got hit by an asteroid, destroy the rocket
        if (collision.gameObject.CompareTag("asteroid"))
        {
            //no longer flying
            flying = false;

            //update anim to destroyed
            anim.Play("Rocket_Destroyed");
            anim.SetBool("destroyed", true);
            anim.SetBool("flying", false);
            
        }
    }

    // Use this for initialization
    void Start () {
        /*
         * initialized default values
         * we are not in space by default
         * parent rocket control aquired
         * we also grab the parent gravity field tracker since both are connected to rocket
         */
        inspace = false;
        rm = GetComponent<RocketMan>();
        gft = rm.gft;
	}
	
	//physics update
    private void FixedUpdate()
    {
        //apply thrust and anim if flying in space
        if (flying)
        {
            //change thrust depending on if we are in space
            if (inspace == false)
            {
                rm.applyThrust(50);
            }else
            {
                rm.applyThrust(0);
            }

            //set flying animation
            anim.SetBool("flying", true);
        }

        //check if effected by any gravitational fields. No! then we are in space
        if (gft.fields_detected.Count <= 0)
        {
            inspace = true;
        } else
        {
            inspace = false;
        }

    }
}
