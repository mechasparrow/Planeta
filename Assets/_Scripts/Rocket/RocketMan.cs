﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//provides behavior for control of soviet rocket
public class RocketMan : MonoBehaviour {

    //Pulls a gravity field tracer to track fields of gravity that the rocket is effected by
    public GravityFieldTracer gft;

    //transform of rocket thrust 
    public Transform exhaust;

    //physics manipulation component
    private Rigidbody2D rb;

    // Use this for initialization
    void Start () {
        //pull a rigidbody for physics manipulations
        rb = GetComponent<Rigidbody2D>();
	}

    //applies a force with such and such magnitude at the nozzle of the rocket (Thrust Angle)
    public void applyThrust(float force)
    {
        Vector2 angleVector = (gameObject.transform.position - exhaust.position).normalized; 
        rb.AddForce(force * angleVector);
    }

    //Turns the rocket a certain torque
    public void turnRocket(float angle)
    {
        rb.AddTorque(angle);
    }

    //physics update
    private void FixedUpdate()
    {

        //Get the closest body according to gravity field tracer
        GameObject targetPlanet = gft.closestBody;

        //grab the magnitude of the rocket velocity
        float vel_mag = gameObject.GetComponent<Rigidbody2D>().velocity.magnitude;

        /**
         * check if rotation must be applied properly 
         * if rocket has enough velocity
         * */
        if (vel_mag > 0.5)
        {
            //rotate the rocket towards the center of the gravitational body with some trig
            Vector3 v_diff = targetPlanet.transform.position - transform.position;
            float atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
            transform.rotation = Quaternion.Euler(0f, 0f, atan2 * Mathf.Rad2Deg + 90);
        }

    }

}
