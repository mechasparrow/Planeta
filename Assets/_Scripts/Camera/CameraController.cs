﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// handles manual control of the camera
public class CameraController : MonoBehaviour {

    //The starting zoom of the camera
    private float defaultZoom;

    //The main game camera
    private Camera cam;

    //amount to zoom by
    public float zoomAmount;

    //min zoom available
    public float minZoom;

	// Use this for initialization
	void Start () {
        //initialized camera variables
        cam = GetComponent<Camera>();
        defaultZoom = cam.orthographicSize;
	}

    // Update is called once per frame
    void Update() {

        //check for keys that will increase and decrease the zoom +/-
        bool increaseZoomKey = Input.GetKeyUp(KeyCode.Equals) || Input.GetKeyUp(KeyCode.Plus);
        bool decreaseZoomKey = Input.GetKeyUp(KeyCode.Minus);

        //key that resets the zoom of the camera i.e backspace
        bool resetZoomKey = Input.GetKey(KeyCode.Backspace);

        //check for camera control key presses

        //reset the zoom
        if (resetZoomKey)
        {
            resetZoom();
        }

        //increase/decrease zoom
        if (increaseZoomKey)
        {
            Debug.Log("increase");
            
            changeZoom(zoomAmount * -1f);
        }
        if (decreaseZoomKey)
        {
            Debug.Log("decrease");
            changeZoom(zoomAmount);
        }

    }

    //changes the zoom of the camera
    void changeZoom(float amount)
    {
        float newsize = cam.orthographicSize;
        newsize += amount;

        //must be greater than min size
        if (newsize >= minZoom)
        {
            //if so then we can change the size
            cam.orthographicSize = newsize;
        }

    }

    //resets the zoom of the camera
    void resetZoom()
    {
        cam.orthographicSize = defaultZoom;
    }
}
