﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script that deals with following objects with the camera (smoothly)
public class CameraFollow : MonoBehaviour {

    //smoothing camera follow variables
    public float dampTime = 0.3f;
    public float maxSpeed = 1.0f;

    //ref velocity
    private Vector2 velocity = Vector2.zero;

    //transform to follow
    public Transform followTransform;

	
	// Update is called once per frame
	void Update () {
		if (followTransform)
        {
            //tries closing the distance between the camera and the transform smoothly
            Vector2 current_position = new Vector2(transform.position.x, transform.position.y);
            Vector2 target_position = new Vector2(followTransform.position.x, followTransform.position.y);

            //camera deltas
            Vector2 delta = target_position - current_position; 
            Vector2 destination = current_position + delta;

            //This makes it nice and smooth
            Vector2 new_position = Vector2.SmoothDamp(current_position, destination, ref velocity, dampTime, maxSpeed, Time.deltaTime);

            //update the camera position
            Vector3 new_camera_position = transform.position;
            new_camera_position = new Vector3(new_position.x, new_position.y, new_camera_position.z);
            transform.position = new_camera_position;

        }
	}
}
