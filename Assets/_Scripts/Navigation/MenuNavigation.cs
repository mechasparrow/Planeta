﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//menu navigation behavior
public class MenuNavigation : MonoBehaviour {

    //key buttons
    public Button btn_LevelSelect, btn_Instructions, btn_Credits;

	// Use this for initialization
	void Start () {
        //binds the buttons
        btn_LevelSelect.onClick.AddListener(startGame);
        btn_Instructions.onClick.AddListener(navigateToInstructions);
        btn_Credits.onClick.AddListener(navigateToCredits);
    }
	
    //navigates to level select page
    public void navigateToLevelSelect()
    {
        Debug.Log("navigating to level select");
        SceneManager.LoadScene(1);
    }

    //starts the game
    public void startGame()
    {
        Debug.Log("navigating to game start");
        SceneManager.LoadScene(3);
    }

    //navigates to instructions page
    public void navigateToInstructions()
    {
        Debug.Log("navigating to instructions");
        SceneManager.LoadScene(2);
    }

    public void navigateToCredits()
    {
        Debug.Log("navigating to credits");
        SceneManager.LoadScene(26);
    }
}
