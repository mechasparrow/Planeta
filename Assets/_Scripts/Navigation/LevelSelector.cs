﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//behavior for level selection
public class LevelSelector : MonoBehaviour {
    
    //list of level selction buttons
    GameObject[] buttons;

    //button for going back
    public Button backButton;

	// Use this for initialization
	void Start () {

        //retrieve all the buttons
        buttons = GameObject.FindGameObjectsWithTag("level_navigation_button");

        //bind each button to the correct load level function
        foreach (GameObject button_go in buttons)
        {
            Button ui_button = button_go.GetComponent<Button>();
            LevelSelectButton lsb = button_go.GetComponent<LevelSelectButton>();
            int btn_level_id = lsb.level_id;
            ui_button.onClick.AddListener(delegate { LoadLevel(btn_level_id); });
        }

        //bind back to menu function
        backButton.onClick.AddListener(BackToMenu);
	}
	
    //Loads the level into the game
    void LoadLevel(int level_id)
    {
        Debug.Log("navigating to a level");
        SceneManager.LoadScene(level_id);
    }

    //Navigates player back to main menu
    void BackToMenu()
    {
        Debug.Log("navigating back to main menu");
        SceneManager.LoadScene(0);
    }
    
}
