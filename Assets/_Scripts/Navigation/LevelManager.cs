﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//manages general level behavior
public class LevelManager : MonoBehaviour {

    //properties for next level to go to and whether level is complete
    public bool level_complete;
    public int next_level_id;

    // for speed running
    public SpeedRunTimer srt;
    public GameObject leaderboard;

	// Update is called once per frame
	void Update () {

        //check if the escape key pressed to escape back to level select
        bool escapeToLevelSelect = Input.GetKey(KeyCode.Escape);

        //check if the enter key pressed for next level
        bool nextLevel = Input.GetKey(KeyCode.Space);
        
        //Restart if you are bad at the game
        bool restart = Input.GetKey(KeyCode.R);

        // checks if leader board is active
        if (leaderboard)
        {
            if (leaderboard.activeSelf == true)
            {
                restart = false;
            }
        }

        if (restart)
        {
            restartLevel();
        }

        //check if user wants to go back to level select
        if (escapeToLevelSelect)
        {
            navigateBackToLevelSelect();
        }

        //check if user wants to go to next level
        if (level_complete)
        {
            if (srt != null)
            {
                Debug.Log("stop the time!");
                if (srt.timerStarted != false) 
                {
                    srt.stopTimer();
                    Debug.Log("stop timer");
                }

                LeaderboardTimes lbt = leaderboard.GetComponentInChildren<LeaderboardTimes>();
                if (lbt != null)
                {
                    lbt.speedRunTime = srt.time_elapsed;
                    SpeedRunLeaderboard srl = leaderboard.GetComponent<SpeedRunLeaderboard>();
                    srl.showLeaderboard();
                }

            }


            if (nextLevel)
            {
                Debug.Log("going to next level");
                gotoNextLevel();
            }
        }

	}

    //restarts the level
    public void restartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // navigate back to level select
    void navigateBackToLevelSelect()
    {
        SceneManager.LoadScene(1);
    }

    // navigates player to next level
    void gotoNextLevel()
    {
        SceneManager.LoadScene(next_level_id);
    }
}
