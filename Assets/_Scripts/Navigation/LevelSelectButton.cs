﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//informational script for level navigation
public class LevelSelectButton : MonoBehaviour {

    //Stores the level id that it is supposed to navigate to
    public int level_id;

}
