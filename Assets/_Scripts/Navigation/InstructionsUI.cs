﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//simple behavior for the instruction screen
public class InstructionsUI : MonoBehaviour {

    //the back button
    public Button backButton;

    // Use this for initialization
    void Start () {
        //bind back to menu function
        backButton.onClick.AddListener(BackToMenu);
    }

    //Navigates player back to main menu
    void BackToMenu()
    {
        Debug.Log("navigating back to main menu");
        SceneManager.LoadScene(0);
    }

}
