﻿using System;

//util class for Json lists
public static class JsonHelper
{
    //parses an object from json
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    //converts an object array to json
    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return UnityEngine.JsonUtility.ToJson(wrapper);
    }

    //makes the wrapper serializable
    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}