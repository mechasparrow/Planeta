﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//behavior that allows asteroids to be explosive
public class ExplosiveAsteroid : MonoBehaviour {

    //explosion prefab
    public GameObject explosion;

    // Tag of what will cause this asteroid to explode
    public string effectedBy;

    //check if the asteroid is hit
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(effectedBy))
        {
            explode();
            Destroy(gameObject);
        }
    }

    //explodes the asteroid
    public void explode()
    {
        GameObject new_explosion = Instantiate(explosion);
        new_explosion.transform.position = gameObject.transform.position;
    }
    
}
