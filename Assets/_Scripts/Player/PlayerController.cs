﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//key behavior script for control of the player
public class PlayerController : MonoBehaviour {

    //variables for UI
    public GameObject resetText;

    //configurable variables regarding movement
    public float playerForce;
    public float jumpForce;
    public float maxGroundVelocity;
    public float friction;
    
    //variables for dealing with jumping game mechanic
    bool canjump = true;
    bool jump;
    float jumptimeout = 0.1f;
    float jumptimer = 0.0f;

    public bool grounded = false;
    private float in_air_timer = 0.0f;
    public float in_air_time_limit;


    //input variables regarding horizontal movement
    bool left;
    bool right;

    public KeyCode jump_key;
    public string HorizontalAxis;

    //Work around for non initialized variables
    private KeyCode actual_jump_key;
    private string ActualHorizontalAxis;


    //direction variable to determine pivoting
    public string dir = "right";

    //For player animation
    public Animator anim;

    //Camera follows the main gravitational body that player is attracted to
    public CameraFollow cam_follow;

    //keeps track of gravitations fields that player is effected by
    public GravityFieldTracer gft;

    // On Game init
    void Start()
    {
        //fail safe if config is missing
        if (HorizontalAxis == null || HorizontalAxis == "")
        {
            HorizontalAxis = "Horizontal";
        }

        if (jump_key == KeyCode.None)
        {
            jump_key = KeyCode.Space;
        }

    }

    // kills the player
    public void KillPlayer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //Game Update to check for keypresses
    private void Update()
    {
        jump = Input.GetKey(jump_key);

    }

    // Update is called once per frame
    void FixedUpdate() {

        //Get horizontal movement
        

        float movementAxis = -Input.GetAxis(HorizontalAxis);

        //grab the closest + strongest gravitational body that effects player
        GameObject targetPlanet = gft.closestBody;

        //tells camera to follow the target planet
        if (targetPlanet != null && cam_follow != null)
        {
            cam_follow.followTransform = targetPlanet.transform;
        }
        
        //Get some planetary/gravity vectors for movement and jumping
        Vector2 referenceVectorFromPlanet = (targetPlanet.transform.position - transform.position).normalized;
        Vector2 perpVector = new Vector2(referenceVectorFromPlanet.y, referenceVectorFromPlanet.x * -1);
        Vector2 MovementForce = perpVector * movementAxis * playerForce;
        
        //Rotate player towards planet when sufficient velocity reached
        float vel_mag = gameObject.GetComponent<Rigidbody2D>().velocity.magnitude;
        if (vel_mag > 0.5)
        {
            Vector3 v_diff = targetPlanet.transform.position - transform.position;
            float atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
            transform.rotation = Quaternion.Euler(0f, 0f, atan2 * Mathf.Rad2Deg + 90);
        }

        //check if grounded for jumping
        RaycastHit2D[] ray_hits = Physics2D.RaycastAll(transform.position, referenceVectorFromPlanet, 0.8f);

        grounded = false;

        //iterates through all the ray casts to see if ground is beneath
        for (int i = 0; i < ray_hits.Length; i++)
        {
            RaycastHit2D rhit = ray_hits[i];

            GameObject obj = rhit.collider.gameObject;

            if (obj.gameObject.CompareTag("player") != true && obj.gameObject.CompareTag("gravity_field") != true)
            {
                grounded = true;
            }
        }

        //Determine whether or not to increment in air timer
        if (grounded == true)
        {
            in_air_timer = 0.0f;
        }
        else
        {
            in_air_timer += Time.deltaTime;
        }

        //  Trigger In Air Reset Dialogue
        if (in_air_timer >= in_air_time_limit)
        {
                
            if (resetText)
            {
                if (resetText.activeInHierarchy == false)
                {

                    resetText.SetActive(true);
                }
            }
        }

        //

        // horizontal movement
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        string current_dir = "";

        // update direction of movement state
        if (movementAxis > 0)
        {
            current_dir = "right";
        } else if (movementAxis < 0) 
        {
            current_dir = "left";
        }

        // walking animations if player walking
        if (movementAxis > 0 || movementAxis < 0)
        {
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }

        // Apply Player Movement Forces
        //check if player is in gravitational field
        if (gft.fields_detected.Count > 0)
        {
            //apply movement force while limited by max velocity
            if (grounded == true && (rb.velocity.sqrMagnitude < (maxGroundVelocity * maxGroundVelocity)))
            {
                rb.AddForce(MovementForce);
            }
            //unless pivot is occuring
            else if (dir != current_dir)
            {
                rb.AddForce(MovementForce);
            }
        }

        //set the direction to the current direction of the physics frame (for pivoting)
        dir = current_dir;

        //Apply friction if player is grounded
        if (grounded)
        {
            rb.AddForce((rb.velocity * -1) * friction * Time.deltaTime, ForceMode2D.Force);
        }

        //jump timer
        if (canjump == false)
        {
            jumptimer += Time.deltaTime;
            if (jumptimer > jumptimeout)
            {
                canjump = true;
                jumptimer = 0.0f;
            }
        }
        
        // vertical movement (jumping) when able to jump
        if (jump == true && grounded == true && canjump == true)
        {
            //Apply jump force normal to gravitational planet vector
            Vector2 jumpVector = -referenceVectorFromPlanet * (jumpForce / 10f);
            rb.angularVelocity = 0f;
            rb.AddForce(jumpVector, ForceMode2D.Impulse);
            canjump = false;
         }

    }
}
