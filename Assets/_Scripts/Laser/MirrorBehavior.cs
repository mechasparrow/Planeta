﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//basic behavior for laser mirrors
public class MirrorBehavior : MonoBehaviour {

    //normal position of the mirror
    public Transform normalPos;

    //vector from the normal position
    public Vector2 normalVector;

	// Use this for initialization
	void Start () {
        //compute the normal vector
        computeNormal();
	}
	
    //computes the normal vector with mag 1
    void computeNormal()
    {
        normalVector = (normalPos.position - gameObject.transform.position).normalized;
    }

    //display the normal for the mirror (DEBUG)
    void displayNormal()
    {
         Debug.DrawLine(transform.position, (Vector2) transform.position - normalVector * 5);
    }
    
	void Update () {
        //compute the normal every frame in case of changes
        computeNormal();
	}
}
