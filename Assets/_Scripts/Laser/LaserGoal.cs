﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//behavior for laser goal
public class LaserGoal : MonoBehaviour {

    //types of goals
    public enum GoalType {
        LASER_BEAM,
        LASER_REG
    };

    //object to trigger and its trigger functionality
    public GameObject triggerObject;
    private Triggerable trigger;

    //color of laser needed to activate
    public Color laserColor;

    //activated or not
    public bool activated;

    //objects that represent the laser in its (de)activated state
    public GameObject unactivatedGoal;
    public GameObject activatedGoal;

    //type of goal
    public GoalType goal_type;

    //timer variables for consistent laser detection
    private float time_elapsed = 0.0f;
    private float laserDeactiveTime = 0.1f;


    private void Start()
    {
        //configure initial goal state
        trigger = triggerObject.GetComponent<Triggerable>();

        if (activated)
        {
            activateLaser();
        }else
        {
            deactivateLaser();
        }
    }

    // apply consistent looped laser code for regular lasers 
    public void Update()
    {
        if (activated)
        {
            if (goal_type == GoalType.LASER_REG)
            {
                time_elapsed += Time.deltaTime;
                if (time_elapsed > laserDeactiveTime)
                {
                    //deactivates laser when not hitting one after a small duration of time
                    deactivateLaser();
                    time_elapsed = 0.0f;
                }
            }
        }
    }

    //activates the laser goal
    public void activateLaser()
    {
        activatedGoal.SetActive(true);
        unactivatedGoal.SetActive(false);
        activated = true;
        
        if (trigger != null)
        {
            trigger.trigger();
        }
        time_elapsed = 0.0f;
    }

    //deactivates the laser goal
    public void deactivateLaser()
    {
        activatedGoal.SetActive(false);
        unactivatedGoal.SetActive(true);
        activated = false;

        if (trigger != null)
        {
            trigger.untrigger();
        }
    }
}
