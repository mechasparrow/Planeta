﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//behavior for laser emitters
public class LaserShooter : MonoBehaviour {

    //line renderer that renders the laser
    private LineRenderer lr;

    //color of the laser
    public Color c;
    
    //starting position of the laser
    public Vector3 startingPosition;

    //max distance the laser can travel
    public float maxDistance;

    //initial laser angle
    public float angle;

    //allowed number of laser hits before it stops trying to bounce off objects
    public int hits;

    //list of laser breakpoints
    public List<Vector2> positions;
    
	void Start () {
        //initial configuration
        lr = gameObject.GetComponent<LineRenderer>();
        lr.colorGradient.mode = GradientMode.Fixed;
        lr.startColor = c;
        lr.endColor = c;
	}

    //raycasts the laser outwards to see when to cut the laser short, will reflect upon collision with mirrors
    void laserRaycast(float angle)
    {
        Vector3 angleVector = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle), 0);
        Vector2 pos = positions[positions.Count - 1];

        RaycastHit2D[] rhs = Physics2D.RaycastAll(pos, angleVector);
        RaycastHit2D rh2d;
        bool hit = false;
        bool raycastAgain = false;
        float newAngle = angle;

        //check through all the objects hit by the raycaster
        foreach (RaycastHit2D rh in rhs)
        {

            //ignore gravitational fields
            if (!rh.transform.gameObject.CompareTag("gravity_field"))
            {
                GameObject hit_object = rh.transform.gameObject;

                //laser kills the player
                if (hit_object.CompareTag("player"))
                {
                    PlayerController pc = hit_object.GetComponent<PlayerController>();
                    pc.KillPlayer();
                }

                //will trigger laser goal on collision
                else if (hit_object.CompareTag("laser_goal"))
                {
                    LaserGoal lg = hit_object.GetComponent<LaserGoal>();
                    if (lg.laserColor == c)
                    {
                        lg.activateLaser();
                    }
                }

                //will reflect upon collision with mirror
                else if (hit_object.CompareTag("mirror"))
                {
                    MirrorBehavior mb = hit_object.GetComponent<MirrorBehavior>();

                    Vector2 incomingLaser = ((Vector2)pos - rh.point);
                    Vector2 perpVector = -1 * new Vector2(mb.normalVector.y, -mb.normalVector.x).normalized;

                    //the new laser vector
                    Vector2 reflectedLaser = Vector2.Reflect(incomingLaser, mb.normalVector);
                    
                    //reflected angle
                    newAngle = Vector2.Angle(Vector2.left, reflectedLaser);

                    //raycast again since it has started new laser segment
                    raycastAgain = true;

                }

                //reassignment 
                rh2d = rh;

                //add laser hit point to list laser break points
                positions.Add(rh2d.point - ((Vector2)angleVector * 0.25f));
                
                //laser has hit something
                hit = true;

                //increase mirror hit count
                hits += 1;

                //end the loop prematurely upon collision
                break;
            }
        }

        //if the laser hits nothing, it will go to its max distance
        if (hit == false)
        {
            positions.Add(pos + ((Vector2)angleVector * maxDistance));
        }

        //will raycast again, as long as it hasn't hit 50 mirror collisions (recursion)
        if (raycastAgain == true && hits < 50)
        {
            laserRaycast(newAngle);
        }
        
    }

    // Update is called once per frame
    void Update() {
        hits = 0;

        //reset laser break points
        positions = new List<Vector2>();
        positions.Add(startingPosition);

        //raycast single ray to start
        laserRaycast(angle);

        //apply laser breakpoints to line renderer
        List<Vector3> positions3d = positions.ConvertAll<Vector3>(p => (Vector3)p);
        lr.positionCount = positions3d.Count;
        lr.SetPositions(positions3d.ToArray());
	}
}
