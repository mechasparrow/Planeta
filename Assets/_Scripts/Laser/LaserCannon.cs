﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//behavior for laser cannons that emit lasers 
public class LaserCannon : MonoBehaviour, Triggerable {

    //get the laser output transform and the pivot point
    public Transform pivotPoint;
    public Transform laserOutTransform;

    //laser emitter
    public LaserShooter ls;

    //cannon activated or not
    public bool activated;
    
	void Start () {
        //initial laser cannon config

        ls.startingPosition = laserOutTransform.position;
        ls.angle = pivotPoint.transform.eulerAngles.z;

        if (activated == true)
        {
            activateLaser();
        }else
        {
            deactivateLaser();
        }
	}
	
    //activates the laser
    public void activateLaser()
    {
        ls.gameObject.SetActive(true);
    }

    //deactivates the laser
    public void deactivateLaser()
    {
        ls.gameObject.SetActive(false);
    }

    /// <summary>
    /// trigger code
    /// </summary>
    public void trigger()
    {
        activateLaser();
    }

    public void untrigger()
    {
        deactivateLaser();
    }
    
    void Update () {
        //updates the laser emitter
        ls.startingPosition = laserOutTransform.position;
        ls.angle = pivotPoint.transform.eulerAngles.z;
    }
    
}
