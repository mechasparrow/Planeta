﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Behavior code for Laser Beams
public class LaserBeam : MonoBehaviour {

    //Angle to shoot at
    public Vector2 shootAngle;

    //Color of laser
    public Color laser_color;

    //Rigidbody
    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        //retrieve rigidbody and set initial shot angle
        rb = gameObject.GetComponent<Rigidbody2D>();
  
    }

    //check for laser collisions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //get the collided gameobject
        GameObject collided_object = collision.gameObject;

        
        //Reflect the laser beam
        if (collided_object.CompareTag("mirror"))
        {

            MirrorBehavior mb = collided_object.GetComponent<MirrorBehavior>();

            Vector2 reflectedVelocity = Vector2.Reflect(shootAngle, mb.normalVector);

            shootAngle = reflectedVelocity;
           
        }
        //Activate Laser Goal
        else if (collided_object.CompareTag("laser_goal"))
        {
            LaserGoal lg = collided_object.GetComponent<LaserGoal>();
            if (lg.laserColor == this.laser_color)
            {
                lg.activateLaser();
            }
            Destroy(gameObject);
        }
        //Destroy the laser gameobject
        else
        {
            Destroy(gameObject);
        }

    }

    //updates the laser angle
    public void updateAngle(Vector2 angleVector)
    {
        Quaternion new_rot = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan2(angleVector.y, angleVector.x));
        transform.rotation = new_rot;
    }

    // Update is called once per frame
    void FixedUpdate () {

        //update the velocity
        rb.velocity = shootAngle;

        //Apply laser rotation
        updateAngle(shootAngle);
    }
}
