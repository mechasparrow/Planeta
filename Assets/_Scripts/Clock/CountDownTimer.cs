﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//behavior specific to timers that count down
public class CountDownTimer : StateMachineBehaviour {

    //once the count up animation ends complete the clock with a count_down state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Clock c = animator.gameObject.GetComponent<Clock>();
        c.clockComplete(Clock.timer_type.COUNT_DOWN);
    }

}
