﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//key clock behavior
public class Clock : MonoBehaviour, Triggerable {

    //pull the clock animation
    private Animator anim;

    //Triggers its own object
    public GameObject ObjectToTrigger = null;
    private Triggerable triggerSystem = null;

    //speed in seconds
    public float time;

    //type of timers
    public enum timer_type
    {
        COUNT_UP,
        COUNT_DOWN
    }

    //timer type selected
    public timer_type clock_type;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        configureClockTime();

        //setup trigger system for triggering and untriggering the triggerable object
        
        if (ObjectToTrigger != null)
        {
            triggerSystem = ObjectToTrigger.GetComponent<Triggerable>();
        }
    }

    //configures the clock animation to be correct duration
    public void configureClockTime()
    {
        float speed_factor = 1f / time;

        anim.SetFloat("count_up_time", speed_factor);
        anim.SetFloat("count_down_time", speed_factor);
    }

    //start the clock
    public void trigger()
    {

        AnimatorStateInfo state_info = anim.GetCurrentAnimatorStateInfo(0);
        bool can_trigger = state_info.IsTag("idle");
        
        //starts count up/count down depending on timer type
        // will untrigger then trigger or vice versa 

        if (can_trigger)
        {
            //will untrigger once down counting up
            if (clock_type == timer_type.COUNT_UP)
            {
                anim.SetTrigger("Start Count Up");
                if (triggerSystem != null)
                {
                    triggerSystem.untrigger();
                }

            }
            //will trigger once count down ends
            else if (clock_type == timer_type.COUNT_DOWN)
            {
                anim.SetTrigger("Start Count Down");
                if (triggerSystem != null)
                {
                    triggerSystem.trigger();
                }
            }
        }

    }

    //clock can not be untriggered
    public void untrigger()
    {
        Debug.Log("Does nothing");
    }

    //behavior once clock is finished
    // either triggers or untriggers triggerable depending on timer_type
    public void clockComplete(timer_type mode)
    {
        if (triggerSystem == null)
        {
            //DO Nothing
            return;
        }
        
        if (mode == timer_type.COUNT_UP)
        {
            triggerSystem.trigger();
        }else if (mode == timer_type.COUNT_DOWN)
        {
            triggerSystem.untrigger();
        }

    }


}
