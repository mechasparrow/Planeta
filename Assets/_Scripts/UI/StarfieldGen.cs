﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Script that generates a starfield for the background
 * */

public class StarfieldGen : MonoBehaviour {
    
    //Star Prefab (Star used for generation of starfield)
    public GameObject star;

    //The options for creating the starfield
    public int starCount;
    public float fieldWidth;
    public float fieldHeight;
    public float z_offset;

    // gens star field on start
    void Start () {
	
        // create and place starCount # of stars
        for (int i = 0; i < starCount; i ++)
        {
            placeStar(randomStarPosition(z_offset));
        }

	}

    /**
     * @returns a random star position
     * @param  z_offset. This is for shifting the stars towards or away from the game camera 
     * */
    Vector3 randomStarPosition(float z_offset)
    {
        // Generate random x and y values for position within star field
        float rando_x = Random.Range(-fieldWidth / 2f, fieldWidth / 2f);
        float rando_y = Random.Range(-fieldHeight / 2f, fieldHeight / 2f);

        // return a random position for the star
        return new Vector3(rando_x, rando_y, z_offset);
    }

    /**
     * Places a star on the game world
     * */
    void placeStar(Vector3 position)
    {
        GameObject star_instance = Instantiate(star, position, Quaternion.identity, GetComponent<Transform>());
    }
}
