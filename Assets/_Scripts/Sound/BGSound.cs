﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSound : MonoBehaviour
{

    public AudioSource bgm; // link to background music prefab, loop, don't play on awake
    

    void Awake()
    {
        GameObject currentBGM = GameObject.FindGameObjectWithTag("bg_sound");
        
        //check if there is some recurrence
        if (currentBGM != null)
        {
            AudioSource audioSource = currentBGM.GetComponent<AudioSource>();
            AudioClip audioClip = audioSource.clip;

            if (audioClip != bgm.clip)
            {
                Destroy(currentBGM);
                currentBGM = null;
            }
        }

        if (currentBGM == null)
        {
            AudioSource spawned = Instantiate(bgm);
            spawned.Play();
            DontDestroyOnLoad(spawned);
        }
    }
}
