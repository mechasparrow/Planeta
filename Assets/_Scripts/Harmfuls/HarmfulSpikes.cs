﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Behavioral code for spikes
public class HarmfulSpikes : MonoBehaviour {

    //When player collides with spikes, kill or hurt him/her
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;
        PlayerController pc = obj.GetComponent<PlayerController>();

        //if the spikes hits the player, kill the player
        if (obj.CompareTag("player") && pc != null)
        {
            pc.KillPlayer();
        }
    }
    
}
