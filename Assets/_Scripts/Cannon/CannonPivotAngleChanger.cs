﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to change the angle of the cannon pivot point by a specific delta
public class CannonPivotAngleChanger : MonoBehaviour, Triggerable {

    //pivot transform
    public Transform pivotPoint;

    //delta angle, angle to change by
    public float angleChange;

    //mode of the angle changer types
    public enum angleMode
    {
        SET,
        DELTA
    }

    //mode of the angle changer
    public angleMode mode;

    /// <summary>
    /// Either sets the angle to the specific launch angle
    /// OR
    /// Changes the angle by the specific angle delta (change)
    /// </summary>
    public void trigger()
    {
        if (mode == angleMode.SET)
        {
            pivotPoint.rotation = Quaternion.Euler(0.0f, 0.0f, angleChange);
        }else if (mode == angleMode.DELTA)
        {
            Vector3 deltaAngles = new Vector3(0.0f, 0.0f, angleChange);
            Vector3 oldAngles = pivotPoint.rotation.eulerAngles;
            Vector3 newAngles = oldAngles + deltaAngles;

            pivotPoint.rotation = Quaternion.Euler(newAngles);
        }

    }

    public void untrigger()
    {
        //Does nothing
    }
    
}
