﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//key behavior for cannons
public class Cannon : MonoBehaviour, Triggerable {

    // force of cannon, where to shoot from, cannon ball to shoot
    public float launchForce;
    public Transform shootTransform;
    public GameObject cannonBall;

    //shoots a cannon ball when triggered
    //only shoots the cannon if it is active (visible)
    public void trigger()
    {
        if (gameObject.activeInHierarchy)
        {
            launchCannon();
        }
    }

    public void untrigger()
    {
        //Does nothing
    }

    //launches the cannon
    void launchCannon()
    {
        //calculate the launch angle vector to apply to the cannon ball
        Vector2 launchVector = shootTransform.position - transform.position;

        //Create a new cannon ball to launch and applies an impulsive force
        GameObject launched_cannon_ball = Instantiate(cannonBall);
        launched_cannon_ball.SetActive(true);
        launched_cannon_ball.transform.position = shootTransform.transform.position;
        launched_cannon_ball.GetComponent<Rigidbody2D>().AddForce(launchVector * launchForce, ForceMode2D.Impulse);
    }
}
