﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//destroys projectiles
public class BallDestroyer : MonoBehaviour {

    // checks if the gameobject is a ball then destroy
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("ball")) {
            //destroys the ball
            Destroy(collision.gameObject);
        }
    }
    
}
