﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows a cannon pivot to switch between a set of angles
/// </summary>
/// 
public class CannonPivotAngleSwitcher : MonoBehaviour, Triggerable {

    //transform of which to rotate properly
    public Transform pivotPoint;

    //angles to switch through
    public float[] angles;

    // current angle index
    private int idx = 0;

    void Start()
    {
        //Set the initial cannon rotation
        pivotPoint.rotation = Quaternion.Euler(0.0f, 0.0f, angles[idx]);
    }

    //When triggered by a button or something else, switch to the next rotation
    public void trigger()
    {
        idx = (idx + 1) % angles.Length;
        pivotPoint.rotation = Quaternion.Euler(0.0f, 0.0f, angles[idx]);
    }

    //Just there to fulfill the interface Triggerable agreement
    public void untrigger()
    {

    }

}
