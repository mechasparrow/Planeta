﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Puzzle Element Button Behavior
public class PuzzleButton : MonoBehaviour {

    //visual colors as to whether button enabled or not
    public Material matEnabled;
    public Material matDisabled;

    //Button indicator (shifts in and out)
    public GameObject buttonIndicator;

    //renderer tied to indicator
    Renderer indicatorRenderer;

    //How much to shift button by
    public float indicatorShift;
    public float shiftedDown;
    public float normalShift;

    //whether button is enabled or not
    public bool buttonEnabled;

    //object to trigger
    public GameObject thingToTrigger;

    //boolean to as to whether button should disable triggerable on walk off
    public bool disableOnWalkOff;

    //button cool down
    public float cooldownTime;
    float timeElapsed = 0.0f;
    bool cooldown = false;
    
    // Use this for initialization
    void Start()
    {
        //initialize variables
        indicatorRenderer = buttonIndicator.GetComponent<Renderer>();
        cooldownTime = 1.0f;
        shiftedDown = buttonIndicator.transform.localPosition.y - indicatorShift;
        normalShift = buttonIndicator.transform.localPosition.y;
    }

    // enables or disabled the button depending on how it is interacted with
    public void buttonInteract()
    {
        enableButton();

    }

    //uninteract with button
    public void buttonUninteract()
    {
        if (enabled == true && disableOnWalkOff == true)
        {
            disableButton();
        }
    }

    //enables the button
    void enableButton()
    {
      

        buttonEnabled = true;
        indicatorRenderer.material = matEnabled;

        //shift the button indicator down
        Vector3 indicatorPosition = buttonIndicator.transform.localPosition;
        indicatorPosition.y = shiftedDown;
        buttonIndicator.transform.localPosition = indicatorPosition;

        //does not trigger anything if the cooldown is true
        if (cooldown == false)
        {
            thingToTrigger.GetComponent<Triggerable>().trigger();
            cooldown = true;
        }

    }

    //disables the button
    void disableButton()
    {
        buttonEnabled = false;
        indicatorRenderer.material = matDisabled;

        //shift the button indicator up
        Vector3 indicatorPosition = buttonIndicator.transform.localPosition;
        indicatorPosition.y = normalShift;
        buttonIndicator.transform.localPosition = indicatorPosition;

        thingToTrigger.GetComponent<Triggerable>().untrigger();
    }

	// Update is called once per frame
	void Update () {
		
        //applies the cooldown timer
        if (cooldown == true)
        {
            timeElapsed += Time.deltaTime;
        }

        if (timeElapsed > cooldownTime)
        {
            cooldown = false;
            timeElapsed = 0.0f;
        }

	}
}
