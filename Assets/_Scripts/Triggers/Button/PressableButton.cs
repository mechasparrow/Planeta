﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sub pressable button (the indicator part)
public class PressableButton : MonoBehaviour {

    //parent button
    public PuzzleButton parentPuzzleButton;
    
    //if the button is hit by the player trigger the button
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("player"))
        {
            parentPuzzleButton.buttonInteract();
        }
    }

    //if player leaves button, un trigger button
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("player"))
        {
            parentPuzzleButton.buttonUninteract();
        }
    }
    
}
