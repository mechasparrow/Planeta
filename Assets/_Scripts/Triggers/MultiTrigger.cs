﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//NOTE comment me

//triggers multiple triggerable things

public class MultiTrigger : MonoBehaviour, Triggerable {

    public GameObject[] objectsToTrigger;

    public void trigger()
    {
        foreach (GameObject go in objectsToTrigger)
        {
            Triggerable t = go.GetComponent<Triggerable>();
            t.trigger();
        }
        
    }

    public void untrigger()
    {
        foreach (GameObject go in objectsToTrigger)
        {
            Triggerable t = go.GetComponent<Triggerable>();
            t.untrigger();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
