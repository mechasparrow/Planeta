﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//key behavior for cannon ball gate
public class BallGate : MonoBehaviour {

    //color of gate
    public string gate_color;

    //versions of gate when hit or unhit
    public GameObject hit_view;
    public GameObject unhit_view;

    //bool as to whether gate is hit or not
    public bool hit;

    //object to trigger
    public GameObject triggerObject = null;
    
    //collision checker
    public void ballGateTrigger(Collider2D collision)
    {
        //if the collisiono is a ball of the correct color, trigger the gameObject
        if (collision.gameObject.CompareTag("ball"))
        {
            CannonBallColor ballColor = collision.gameObject.GetComponent<CannonBallColor>();
            if (ballColor.color == gate_color)
            {
                Debug.Log("Correct Ball");
                hit = true;
            
                //triggers the object when it is the correct ball color
                if (triggerObject != null)
                {
                    triggerObject.GetComponent<Triggerable>().trigger();
                }

                //update the hit/unhit view of the gate
                updateView();
            }else
            {
                Debug.Log("Invalid Ball");
            }
        }
    }

    //updates the view to the visual rep of hit/unhit
    void updateView()
    {
        if (hit == true)
        {
            hit_view.SetActive(true);
            unhit_view.SetActive(false);
        }
        else if (hit == false) 
        {
            hit_view.SetActive(false);
            unhit_view.SetActive(true);
        }
    }

}
