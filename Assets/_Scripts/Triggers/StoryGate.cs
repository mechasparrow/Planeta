﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used for triggering things in the story
public class StoryGate : MonoBehaviour {

    //object to trigger
    public GameObject triggerableObject;

    //trigger the object once an object passes through the gate
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Triggerable triggerableSystem = triggerableObject.GetComponent<Triggerable>();
        triggerableSystem.trigger();
    }
    
}
