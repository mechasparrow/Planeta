﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to track multiple goal gates and triggers when all gates passed
public class MultiGoalGateTracker : MonoBehaviour, Triggerable {

    //types of gate to track
    public enum color_gate {
        BLUE,
        RED,
        BOTH
    }

    //the object to trigger upon completion
    public GameObject objectToTrigger = null;

    //gates to track (defaults to Both types)
    public color_gate tracker_color = color_gate.BOTH;

	// Use this for initialization
	void Start () {

        //gets the current count of gates untriggered
        int count = get_gate_count();

        //if all are triggered,trigger the gameobject
        if (count == 0)
        {
            triggerObject();
        }

	}
    
    //trigger the object that needs triggering
    void triggerObject()
    {
        Triggerable objectTrigger = objectToTrigger.GetComponent<Triggerable>();
        objectTrigger.trigger();
    }

    //get the amount of gates untriggered
    public int get_gate_count()
    {

        int ball_gate_count = 0;
        GameObject[] gates = GameObject.FindGameObjectsWithTag("ball_gate");
        Debug.Log(gates.Length);
        
        foreach (GameObject g in gates)
        {
            //get the ball gate
            BallGate bg = g.GetComponent<BallGate>();
            string ball_gate = "";

            //set the gate color to check for
            if (tracker_color == color_gate.BLUE)
            {
                ball_gate = "blue";
            }
            else if (tracker_color == color_gate.RED)
            {
                ball_gate = "red";
            }
            else
            {
                ball_gate = null;
            }
            

            //adds one to the count if a untriggered gate is found to be part of the search criteria
            if (bg.hit == false)
            {
                if (bg.gate_color == ball_gate)
                {
                    ball_gate_count += 1;
                }
                else if (ball_gate == null)
                {
                    ball_gate_count += 1;
                }
            }
            
        }

        //return the ball count
        return ball_gate_count;
    }

    //Updates and checks if any gates are left upon a gate activation
    public void trigger()
    {
        
        GameObject[] gates = GameObject.FindGameObjectsWithTag("ball_gate");
        //get amount of gates untriggered
        int gate_count = get_gate_count();

        //no gates left, trigger the object
        if (gate_count == 0)
        {
            triggerObject();
        }
    }

    public void untrigger()
    {
        //Does nothing
        Debug.Log("does nothing");
    }
}
