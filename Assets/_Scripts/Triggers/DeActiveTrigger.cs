﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//simple triggerable that activates/deactivates a gameobject
//Does opposite of ActiveTrigger Script
public class DeActiveTrigger : MonoBehaviour, Triggerable {

   
    //deactivates the gameobject
    public void trigger()
    {
        gameObject.SetActive(false);
        
    }

    //activates the gameobject
    public void untrigger()
    {
        gameObject.SetActive(true);
    }
    
}
