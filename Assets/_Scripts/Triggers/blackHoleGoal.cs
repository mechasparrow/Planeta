﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//key behavior for black hole behavior 
public class blackHoleGoal : MonoBehaviour, Triggerable {

    //overarching level manager
    public LevelManager manager;

    //blackhole activated and visible?
    public bool activated;

    //text/gameObject to activate/trigger
    public Text winText;
    public GameObject winObject;

	// Use this for initialization
	void Start () {
        //activates/deactivates based on base activated bool set
        if (activated)
        {
            activateGoal();
        }else
        {
            deactivateGoal();
        }
	}

    //on a object collision with the black hole, the object is destroyed. If a goal object, trigger the goal
    public void OnCollisionEnter2D(Collision2D collision)
    {
        //destroy the object
        Destroy(collision.gameObject);

        //check if the gameobject is player + rocket. If so enable the object/text
        if (collision.gameObject.CompareTag("player") || collision.gameObject.CompareTag("rocket"))
        {
            //if there is a Object,enable it
            if (winObject != null)
            {
                winObject.SetActive(true);
            }

            //if we have a win text, enable it
            if (winText != null )
            {

                winText.gameObject.SetActive(true);
            }

            //notify the level manager
            if (manager != null)
            {
                manager.level_complete = true;
            }

        }
    }

    //activates the goal
    public void activateGoal()
    {
        
        gameObject.SetActive(true);

        //activate the goal
        activated = true;
        
    }

    //deactivates the goal
    public void deactivateGoal()
    {
        gameObject.SetActive(false);
        activated = false;
    }
    
    //trigger the goal whilst activating the goal
    public void trigger()
    {
        if (activated == false)
        {
            activateGoal();
        }
    }

    //untrigger the goal
    public void untrigger()
    {
        deactivateGoal();
    }
}
