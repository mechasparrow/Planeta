﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//simple gate trigger that activates the ball gate once a ball enters through the region
public class GateTrigger : MonoBehaviour {

    //parent ball gate
    public BallGate ballGate;

    //passes the collision up to the ball gate
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ballGate.ballGateTrigger(collision);   
    }

}
