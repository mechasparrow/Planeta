﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//simple triggerable that activates/deactivates a gameobject
public class ActiveTrigger : MonoBehaviour, Triggerable {

   
    //activates the gameobject
    public void trigger()
    {
        gameObject.SetActive(true);
        
    }

    //deactivates the gameobject
    public void untrigger()
    {
        gameObject.SetActive(false);

    }
    
}
