﻿
//simple interface for objects that can be triggered and untriggered
public interface Triggerable {

    void trigger();
    void untrigger();
}
