﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Tracks the objects the enter a gravitational body's field
public class GravityFieldTracker : MonoBehaviour {

    //main gravity field of gravitational body
    public GravityField gf;
    
    //When an object (RigidBody2d) enters the gravitational field, apply the gravitational acceleration to it
    private void OnTriggerStay2D(Collider2D collision)
    {
        GameObject go = collision.gameObject;

        if (go.GetComponent<Rigidbody2D>())
        {
            gf.applyGravity(go);
        }
    }
    
}
