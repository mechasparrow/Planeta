﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//gravity field behavior of gravitational body
public class GravityField : MonoBehaviour {

    //gravitational acceleration that is applied to all objects that enter the field
    public float gravity_acceleration;
    
	//applies gravity towards the center of the gravitational body to whatever poor object enters the gravitational field
    public void applyGravity(GameObject go)
    {
        Vector2 normalized_planet = (transform.position - go.transform.position).normalized;
        
        Vector2 gravity_vector = normalized_planet.normalized * (go.GetComponent<Rigidbody2D>().mass * gravity_acceleration);
  
        if (go.CompareTag("nogravity") != true)
        {
            go.GetComponent<Rigidbody2D>().AddForce(gravity_vector);
        }
    }
    
}
