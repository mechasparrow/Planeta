﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//applies an initial angular velocity to a rigidbody2d object in the game
public class InitialAngularVelocity : MonoBehaviour {

    //initial rotational velocity
    public float rotationVel;

	// Use this for initialization
	void Start () {

        //pulls the rigidbody and sets the angular velocity
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.angularVelocity = rotationVel;

	}
	
}
