﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Traces (Tracks) the gravitational fields that the object is effected by

public class GravityFieldTracer : MonoBehaviour {

    //list of fields detected
    public List<GameObject> fields_detected = new List<GameObject>();

    //closest + strongest gravitational body out of all fields detected
    public GameObject closestBody;

    //Add gravitational fields once they start attracting the object
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<GravityFieldTracker>())
        {
            GameObject other = collision.gameObject.GetComponent<GravityFieldTracker>().gf.gameObject;

            if (fields_detected.Contains(other) != true)
            {
                fields_detected.Add(other);
            }

        }

    }

    //Remove gravitational fields once they stop attracting the object
    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.GetComponent<GravityFieldTracker>() == null)
        {
            return;
        }

        GameObject other = collision.gameObject.GetComponent<GravityFieldTracker>().gf.gameObject;

        if (fields_detected.Contains(other))
        {
            fields_detected.Remove(other);
        }

    }
    
    //physics update
    private void FixedUpdate()
    {

        //strongest strength initially zero(everything is stronger than this)
        float strongestStrength = 0.0f;

        //go through each of the gravitational fields detected until the closest and strongest one is found
        // distance * strength heuristic since they are directly proportional for the sake of spherical cow
        //could be optimized
        foreach (GameObject go in fields_detected)
        {
            //checks if the game object is actually activated
            if (go.activeSelf == false)
            {
                continue;
            }

            float distance = Vector2.Distance(go.transform.position, transform.position);
            float gravitation_strength = go.GetComponent<GravityField>().gravity_acceleration;
            float strength = gravitation_strength * (1 / (distance * distance));

            if (strength > strongestStrength)
            {
                closestBody = go;
                strongestStrength = strength;
            }

        }

    }

}
