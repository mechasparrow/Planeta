﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Applies an initial velocity for a rigidbody2d
public class InitialVelocity : MonoBehaviour {

    //grab the initial velocity to use
    public Vector2 initialVelocity;

	// Use this for initialization
	void Start () {

        //grab the rigidbody2d and set its initial velocity
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = initialVelocity;

	}
	
}
