﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Behavior that forces object into orbit
public class OrbitalRegion : MonoBehaviour {

    //planet it is part of
    public GameObject parentObject;

    //angle to release the projectile
    public float releaseAngle;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //get the collided object
        GameObject go = collision.gameObject;

        //check if it does not a orbital velocity effecting it
        if (go.GetComponent<InitialOrbitalVelocity>() != null)
        {
            return;
        }

        //create a new orbital velocity for the object
        InitialOrbitalVelocity iv = go.AddComponent<InitialOrbitalVelocity>();

        //configure the orbital velocity component 
        Rigidbody2D rb = go.GetComponent<Rigidbody2D>();
        iv.gravitational_body = parentObject;

        if (releaseAngle != 0.0f)
        {
            iv.disable_angle = true;
            iv.angle = releaseAngle;
        }else
        {
            iv.disable_angle = false;
        }

        //calc orbital direction

        Vector2 gravitation_vector = (transform.position - iv.gravitational_body.transform.position).normalized;
        Vector2 origPerpVector = new Vector2(gravitation_vector.y, gravitation_vector.x * -1).normalized;

        //checks whether it must be set to clockwise or counter clockwise
        float projectile_angle = Vector2.Angle(origPerpVector, rb.velocity);
        
        if (projectile_angle > 90)
        {
            iv.orbitalDirection = "counterclockwise";
        }else if (projectile_angle < 90)
        {
            iv.orbitalDirection = "clockwise";
        }

    }
    
}
