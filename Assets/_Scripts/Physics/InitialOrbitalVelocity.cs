﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for perfect orbital velocities
public class InitialOrbitalVelocity : MonoBehaviour {

    //orbital body
    public GameObject gravitational_body;

    //radius to orbit
    public float orbital_radius;

    //rigidbody of projectile for physics
    private Rigidbody2D rb;

    //direction to orbit
    public string orbitalDirection;

    //tangential velocity necessary to orbit planet
    private float tangential_velocity;

    // variables necessary to fly off at a specific angle
    private float orbital_time;
    private float time_elapsed = 0f;
    public bool disable_angle;
    private string origTag;

    //angle to fly off at
    public float angle;

	// Use this for initialization
	void Start () {
        //rigid body of projectile
        rb = GetComponent<Rigidbody2D>();

        //get the gravitational accel
        float body_acceleration = gravitational_body.GetComponent<GravityField>().gravity_acceleration;
        origTag = gameObject.tag;
        orbital_radius = calcRadius();

        //compute vector from projectile to gravitational body
        Vector2 origVector = (transform.position - gravitational_body.transform.position).normalized;

        //compute needed tangential velocity
        tangential_velocity = Mathf.Sqrt(orbital_radius * body_acceleration);

        //Place projectile radius away from center
        Vector3 new_pos = gameObject.transform.position;
        new_pos.x = gravitational_body.transform.position.x + (origVector.x * orbital_radius);
        new_pos.y = gravitational_body.transform.position.y + (origVector.y * orbital_radius);
        gameObject.transform.position = new_pos;

        //calc perp vector
        Vector2 origPerpVector = new Vector2(origVector.y, origVector.x * -1).normalized;

        Vector2 perpVector = origPerpVector;

        //change the perp vector if counter clockwise
        if (orbitalDirection == "clockwise")
        {
            perpVector = origPerpVector;
        }
        else if (orbitalDirection == "counterclockwise")
        {
            perpVector = -origPerpVector;
        }
        

        //Apply initial tangential velocity
        rb.velocity = perpVector * tangential_velocity;

        // Calculates time required to disable the orbit
        if (disable_angle == true)
        {
            float radian_angle = angle * ((2 * Mathf.PI) / 360);
            orbital_time = (orbital_radius * radian_angle) / tangential_velocity;
        }else
        {
            Destroy(this);
        }
    }

    //calcs radius in body
    float calcRadius()
    {
        return Vector2.Distance(gravitational_body.transform.position, transform.position);
    }

	// Update is called once per frame
	void FixedUpdate () {

        //disabled gravity of planet temporarily to fly off tangentially
        if (disable_angle == true)
        {
            time_elapsed += Time.fixedDeltaTime;

            if (time_elapsed > orbital_time)
            {
                gameObject.tag = "nogravity";
                time_elapsed = 0.0f;
                disable_angle = false;
            }
        }

        //sets it back to original tag after half a second
        if (gameObject.tag == "nogravity")
        {
            time_elapsed += Time.fixedDeltaTime;
            if (time_elapsed > 0.5f)
            {
                gameObject.tag = origTag;
                Destroy(this);
            }
        }
        
    }
}
