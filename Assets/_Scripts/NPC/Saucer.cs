﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code that deals with flying saucer behavior
public class Saucer : MonoBehaviour {

    //Actual Laser for Saucer to shoot
    public GameObject laserToShoot;

    //Velocity at which to shoot the laser
    public float laserVelocity;

    //How far the laser should initially be shot from the saucer
    private float offsetFromSaucer = 1.75f;

	
    //shoots a laser at a specified angle
    public void shootLaser(float angle)
    {
        // base 2d vector components
        float x_angle_base = Mathf.Cos(angle * Mathf.Deg2Rad);
        float y_angle_base = Mathf.Sin(angle * Mathf.Deg2Rad);

        //base angle vector
        Vector2 shoot_angle_base = new Vector2(x_angle_base, y_angle_base);

        // instantiate the prefab
        // assign the velocity vector (amp of mag)
        // have it start from center of saucer with some offset
        GameObject new_laser = Instantiate(laserToShoot);
        new_laser.GetComponent<LaserBeam>().shootAngle = shoot_angle_base * laserVelocity;
        new_laser.GetComponent<LaserBeam>().updateAngle(shoot_angle_base);
        new_laser.transform.position = gameObject.transform.position + (Vector3)(shoot_angle_base * offsetFromSaucer);
        new_laser.SetActive(true);
    }

}
