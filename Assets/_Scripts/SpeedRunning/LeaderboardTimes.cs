﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System;
using System.Linq;

// Leaderboard Behavior for saving, loading, and viewing leader board times
public class LeaderboardTimes : MonoBehaviour {

    // Speedrun Record Object Def

    [Serializable]
    public class Record : IComparable<Record>
    {
        //Gamer Tag of Player
        public string name;

        //time in seconds
        public float time;
            
        //constructor
        public Record(string name, float time)
        {
            this.name = name;
            this.time = time;
        }

        // For sorting
        public int CompareTo(Record other)
        {
            if (this.time == other.time)
            {
                return 0;
            }else if (this.time < other.time)
            {
                return -1;
            }else // (this.time > other.time)
            {
                return 1;
            }
        }

    }

    //Record list
    public List<Record> records = new List<Record>();
    private string dataPath;


    // Leaderboard array
    
    private Text[] leaderboard_list = new Text[5];

    [SerializeField]
    Text text1, text2, text3, text4, text5;

    // last speed run time
    public float speedRunTime = 0.0f;

    // name input
    public Text nameInput;

    // Use this for initialization
    void Start () {

        //init leaderboard list (text view list)
        leaderboard_list = new Text[5]
        {
            text1, text2, text3, text4, text5
        };

        //calc specific records path for the level at play
        dataPath = Path.Combine(Application.persistentDataPath, "records_" + SceneManager.GetActiveScene().name + ".json");

        //list of records
        records = new List<Record>(loadRecords(dataPath));

        //display the loaded records
        displayRecords();
    }
	
    //list of dummy records
    List<Record> dummyRecords()
    {
        List<Record> dRecords = new List<Record>();

        dRecords.Add(new Record("BOB", 100.0f));
        dRecords.Add(new Record("ROB", 100.0f));
        dRecords.Add(new Record("BOB", 20.0f));
        dRecords.Add(new Record("BOB", 100.0f));
        dRecords.Add(new Record("ROSS", 50.0f));
        
        return dRecords;
        
    }

    //clears the records
    public void clearRecords()
    {
        records = new List<Record>();
        LeaderboardTimes.saveRecords(records.ToArray(), dataPath);
        displayRecords();
    }

    //Creates a new record
    public void newRecord()
    {
        //create and add a new record
        float time = speedRunTime;
        string name = nameInput.text;
        Record new_record = new Record(name, time);
        records.Add(new_record);
    
        //sort the records by fastest time
        records.Sort();

        //save and display records
        LeaderboardTimes.saveRecords(records.ToArray(), dataPath);
        displayRecords();
    }

    //loads in records from a specific path
    static Record[] loadRecords(string path)
    {
        try
        {

            using (StreamReader streamReader = File.OpenText(path))
            {
                string jsonString = streamReader.ReadToEnd();
                Debug.Log(jsonString);
                return JsonHelper.FromJson<Record>(jsonString);
            }
        }catch (System.IO.FileNotFoundException err)
        {
            return new Record[] { };
        }
    }

    //Saves the records to a specific path
    static void saveRecords(Record[] records, string path)
    {
        Debug.Log(records.Length);
        string jsonString = JsonHelper.ToJson<Record>(records);
        Debug.Log(jsonString);
        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(jsonString);
        }

    }
    
    //displays the records
    void displayRecords()
    {

        for (int i = 0; i < 5; i++)
        {
            
            Text t = leaderboard_list[i];
            t.text = "NONE";
        }

        for (int i = 0; i < 5; i ++)
        {
            if (i + 1 > records.Count)
            {
                break;
            }

            Debug.Log(records[i]);

            Text t = leaderboard_list[i];
            t.text = records[i].name + " : " + SpeedRunTimer.toTimerText(records[i].time);


        }

    }
}
