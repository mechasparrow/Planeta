﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// used to display final speed run time
/// </summary>
public class FinalSpeedRunTime : MonoBehaviour {

    //speed run timer
    public SpeedRunTimer srt;

    //phrase to display when run completed
    public string speedRunPhrase;

    //text used for display
    private Text displayText;
    
	void Start () {
        //config display text
        displayText = GetComponent<Text>();
	}
	
    // NOTE timer will be stopped when this is visible
	void Update () {
        //update the display text when ready
        displayText.text = speedRunPhrase + srt.timer_text;
	}
}
