﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Timer for speed running
public class SpeedRunTimer : MonoBehaviour {

    //display for the timer
    private Text speedRunDisplay;

    //time variables
    public string timer_text = "";
    public bool timerStarted = true;

    public float time_elapsed = 0.0f;

	// Use this for initialization
	void Start () {
        //initial config
        speedRunDisplay = GetComponent<Text>();		
	}

    //Converts time to timer text
    public static string toTimerText(float time)
    {
        int seconds = Mathf.FloorToInt(time);
        int minutes = Mathf.FloorToInt(seconds / 60.0f);
        int milliseconds = Mathf.FloorToInt((time * 100.0f) % 100);
            
        seconds = seconds - (minutes * 60);

        //format the minutes and seconds
        string seconds_string = seconds.ToString("00");
        string minutes_string = minutes.ToString("00");
        string milliseconds_string = milliseconds.ToString("00");
       
        string time_string = minutes_string + ":" + seconds_string + ":" + milliseconds_string;    
        return time_string;
    }

    //Displays the time onto the speed run timer
    public void displayTime()
    {
        timer_text = toTimerText(time_elapsed);
        speedRunDisplay.text = "Time: " + timer_text;
    }

    //stops the timer
    public void stopTimer()
    {
        timerStarted = false;
    }
    
	void Update () {
        //increment the timer when running
        if (timerStarted)
        {
            time_elapsed += Time.deltaTime;
            displayTime();
        }
	}

}
