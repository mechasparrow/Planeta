﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Behavior code for speed run leaderboard
public class SpeedRunLeaderboard : MonoBehaviour {

    //grab the leaderboard buttons
    public Button show_button;
    public Button hide_button;
    public Button restart_button;
    public Button clear_button;
    
	void Start () {
        //add the click listeners for each of the buttons
        show_button.onClick.AddListener(showLeaderboard);
        hide_button.onClick.AddListener(hideLeaderboard);
        restart_button.onClick.AddListener(restartGame);
        clear_button.onClick.AddListener(clearScoreboard);

        //initially disable the leaderboard view
        gameObject.SetActive(false);
    }

    //clears the score board
    public void clearScoreboard()
    {
        //clears all the records on the score board
        LeaderboardTimes lbt = GetComponentInChildren<LeaderboardTimes>();
        lbt.clearRecords();
    }

	//restarts the game
    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //Hides the leaderboard
	public void hideLeaderboard()
    {
        gameObject.SetActive(false);
        show_button.gameObject.SetActive(true);
    }
    
    //Show the leaderboard
    public void showLeaderboard()
    {
        gameObject.SetActive(true);
        show_button.gameObject.SetActive(false);
    }
}
