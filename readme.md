# Planeta (Orbital Physics Video Game)
In Planet, you are Yuri, an astronaut for the space federation of the U.S.S.R. You went out on a mission but something went wrong! You need to find a way back.

You can play the game over at 
https://planeta.netlify.com/

# Instructions
- Left and Right Arrow Keys to Move
- R to Reset the Level (Probably a whole bunch)
- Space to Jump
- Plus and Minus Keys to Change Camera Zoom
- Backspace to Reset Zoom
- Use the Escape key to exit a Level

# Tools Used
[Unity Game Engine](https://unity3d.com/)
[Piskel Pixel Art Creator](https://www.piskelapp.com/)

# Video Game Inspirations
## Angry Birds Space:
Got the idea about gravitational fields from it. Helped me not completely try modeling all physics by hand

## Super Mario Galaxy
One my most favorite games I played as a child. Was the key inspiration for this game. Contributed to the core mechanic of jumping from planet to planet.

## My AP Physics Teacher (Mrs.Wilke)
She taught me just about everything I currently know about physics, including the physics that were used in this game.

## Merrick
Helped me develop the story out and gave critiques on artwork

## Rest of Spark group + Family
Moral support.
